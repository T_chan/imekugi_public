# iMekugi_public

iMekugi is a replacement solution for the old DOS/Windows ASPI drivers that allowed connecting SCSI devices to a host computer.

## Warnings

- [ ] Use at your own risk. No guarantees made.
- [ ] No security is provided for the packets going via TCP, so this is not secure whatsoever.
- [ ] If you expose your SCSI device to somebody else via TCP, be aware that this other person could ruin your hardware/software/data/anything in your life and more.

## Possibilities

iMekugi can:
- [ ] transfer SCSI commands from any DOS/Windows application
    - running in a Virtual Machine, to any Windows or Linux host PC via TCP (so also a different pc as host)
    - running on a PC A, to any other PC B running Windows or Linux via TCP
- [ ] act as the old ASPI driver, but also running on more recent Windows version (XP/7/8/10/11)

![iMekugi Networking possibilities](docs/iMekugiAspi.jpg)


## How to use

- [ ] To use it in a MS-DOS Virtual Machine: TODO - NOT IMPLEMENTED YET
- [ ] [ ] Test
- [ ] To use it in a Windows Virtual Machine: TODO
- [ ] To use it on a Windows PC to transfer SCSI commands to another PC: TODO


## TODO REMOVE THIS

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
```
TODO REMOVE THIS
```
